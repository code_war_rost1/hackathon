/*
 * node.h
 *
 *  Created on: 15-Sep-2019
 *      Author: sunbeam
 */

#ifndef NODE_H_
#define NODE_H_

typedef struct node
{
	int id;
	char user_name[32];
	char user_email[32];
	long int user_phone;
	char password[8];
	struct node *prev;
	struct node *next;
}node_t;



#endif /* NODE_H_ */
