/*
 * main.c
 *
 *  Created on: 09-Sep-2019
 *      Author: sunbeam
 */

#include<stdio.h>
#include<stdlib.h>
#include"user_list.h"

int main(void)
{
	user_list_t l1;
	init_list(&l1);
	user_t *t;
	
	add_node_at_last_position(&l1,1,"shrey kawadiya","shrey",6548956789,"sht@12");
	add_node_at_last_position(&l1,2,"shripal kawadiya","shri",123456789,"sk@1235");

	add_node_at_first_position(&l1,2,"shri kawadiya","shrdsci",126789,"sk@35");
	
	t = search_node_by_email(&l1,"shrdsci","sk@35");
	if(t==NULL)
		printf("user not found\n");
	else
 		display_user(t);

	display_list(&l1);

	return 0;
}


