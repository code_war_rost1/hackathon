/*
 * node.h
 *
 *  Created on: 15-Sep-2019
 *      Author: sunbeam
 */

#ifndef USER_H_
#define USER_H_

typedef struct user
{
	int id;
	char user_name[32];
	char user_email[32];
	long int user_phone;
	char user_password[8];
	struct user *prev;
	struct user *next;
}user_t;



#endif /* NODE_H_ */
