/*
 * list.c
 *
 *  Created on: 15-Sep-2019
 *      Author: sunbeam
 */
#include<stdio.h>
#include<stdlib.h>
#include"user_list.h"
#include<string.h>


void init_list(user_list_t *list)
{
	list->head = NULL;
	list->cnt = 0;
}

int is_list_empty(user_list_t *list)
{
	return ( list->head == NULL );
}

user_t *create_user( int id, char *user_name, char *user_email, long int user_phone,char *user_password)
{
	user_t *temp = (user_t *)malloc(sizeof(user_t));
	if( temp == NULL)
	{
		perror("malloc() failed !!!\n");
		exit(1);
	}
	temp->id = id;
	strcpy(temp->user_name,user_name);
	strcpy(temp->user_email,user_email);
	strcpy(temp->user_password,user_password);
	temp->user_phone = user_phone;
	temp->prev = NULL;
	temp->next = NULL;

	return temp;
}

void add_node_at_last_position(user_list_t *list, int id, char *user_name, char *user_email, long int user_phone,char *user_password)
{
	//create a newnode
	user_t *newnode = create_user( id, user_name, user_email, user_phone, user_password);

	//if list empty -- attach newly created node to the head
	if( is_list_empty(list))
	{
		list->head = newnode;
		list->head->next = list->head;
		list->head->prev = newnode;
		list->cnt++;
	}
	else//if list is not empty
	{
		//store the addr of first node into the next part of newly created node
		newnode->next = list->head;
		//store the addr of cur last node into the prev part of newly created node
		newnode->prev = list->head->prev;
		//store the addr of newly created node into the next part of cur last node
		list->head->prev->next = newnode;
		//store the addr of newly created node into the prev part of first node
		list->head->prev = newnode;
		list->cnt++;
	}
}


void display_user(user_t *user)
{
	printf("id=%4d, user_name = %s , user_email = %s , user_phone = %ld , password = %s \n", user->id, user->user_name, user->user_email, user->user_phone, user->user_password);
}

void display_list(user_list_t *list)
{
	//start the traversal from the first node
	user_t *trav = list->head;
	do
	{
		printf("id=%4d, user_name = %s , user_email = %s , user_phone = %ld , password = %s \n",trav->id, trav->user_name, trav->user_email, trav->user_phone, trav->user_password);
		trav = trav->next;
	}while( trav != list->head );
	printf("\n");
}

void delete_node_at_first_position(user_list_t *list)
{
	//if list is not empty
	if( !is_list_empty(list))
	{
		//if list contains only one node
		if( list->head == list->head->next )
		{
			//delete the node and make head as NULL & cnt as 0
			free(list->head);
			list->head = NULL;
			list->cnt = 0;
		}
		else//if list contains more than one nodes
		{
			//store the addr of cur first node which to be deleted in a temp
			user_t *temp = list->head;
			//store the addr of cur second node into the head
			list->head = list->head->next;
			//store the addr of last node into the prev part of new first node
			list->head->prev = temp->prev;
			//update next part of last node to the new first node
			temp->prev->next = list->head;

			//delete the node
			free(temp);
			temp = NULL;
			list->cnt--;
		}

	}
	else
		printf("list is empty !!!\n");
}

void delete_node_at_last_position(user_list_t *list)
{
	//if list is not empty
	if( !is_list_empty(list))
	{
		//if list contains only one node
		if( list->head == list->head->next )
		{
			//delete the node and make head as NULL & cnt as 0
			free(list->head);
			list->head = NULL;
			list->cnt = 0;
		}
		else//if list contains more than one nodes
		{
			//store the addr of cur last node which to be deleted in a temp
			user_t *temp = list->head->prev;

			//store the addr of first node into the prev part of cur second last node
			temp->prev->next = list->head;
			//store the addr of cur second last node into the prev part of first node
			list->head->prev = temp->prev;
			//delete the node
			free(temp);
			temp = NULL;

			list->cnt--;
		}

	}
	else
		printf("list is empty !!!\n");
}

void add_node_at_first_position(user_list_t *list, int id, char *user_name, char *user_email, long int user_phone,char *user_password)
{
	//create a newnode
	user_t *newnode = create_user(id, user_name, user_email, user_phone, user_password);

	//if list empty -- attach newly created node to the head
	if( is_list_empty(list))
	{
		list->head = newnode;
		list->head->next = list->head;
		list->head->prev = newnode;
		list->cnt++;
	}
	else//if list is not empty
	{
		//store the addr of cur first node into next part of newly created node
		newnode->next = list->head;
		//store the addr of last node into the prev of newly created node
		newnode->prev = list->head->prev;
		//store the addr of newly created node into the prev part of cur first node
		list->head->prev = newnode;
		//store the addr of newly created node into the head
		list->head = newnode;
		//update next part of last node with updated value of head
		list->head->prev->next = list->head;
		list->cnt++;
	}
}

user_t* search_node_by_email(user_list_t *list,char* user_email,char* user_password)
{
	user_t *temp = NULL;
	user_t *trav= list->head;
	int i = list->cnt;
	while(i!=0)
	{
		if((strcmp(trav->user_email,user_email) && strcmp(trav->user_password,user_password))==0)
			break;
		i--;
		trav=trav->next;
	}
	if(i!=0)
	{
		temp=trav;
		return temp;
	}
	else
		return temp;
}


void free_list(user_list_t *list)
{
	if( !is_list_empty(list))
	{
		while( !is_list_empty(list))
			delete_node_at_last_position(list);

			//delete_node_at_first_position(list);
		printf("list freed successfully...\n");
	}
	else
		printf("list is empty !!!\n");
}
