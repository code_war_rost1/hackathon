/*
 * list.h
 *
 *  Created on: 15-Sep-2019
 *      Author: sunbeam
 */

#ifndef USER_LIST_H_
#define USER_LIST_H_

#include"user.h"

typedef struct user_list
{
	user_t *head;
	int cnt;
}user_list_t;

void display_node(user_t *user);
void init_list(user_list_t *list);
int is_list_empty(user_list_t *list);
void add_node_at_last_position(user_list_t *list, int id, char *user_name, char *user_email, long int user_phone,char *user_password);
void add_node_at_first_position(user_list_t *list,int id, char *user_name, char *user_email, long int user_phone,char *user_password);
user_t* search_node_by_email(user_list_t *list,char* user_email,char* user_password);
void delete_node_at_first_position(user_list_t *list);
void delete_node_at_last_position(user_list_t *list);
void display_list(user_list_t *list);
void free_list(user_list_t *list);


#endif /* LIST_H_ */
